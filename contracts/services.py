from . import models
from datetime import datetime
from .utils import ContractUtils
import csv
from io import TextIOWrapper
from collections import defaultdict
from mec_energia.error_response_manage import * 
import datetime
import math
from decimal import Decimal

class ContractServices:  
    def get_file_errors(self, csv_reader, consumer_unit_id): 
        energy_bill_data = []
        date_list = []
        duplicate_dates = []
        seen_dates = set()

        for index, row in enumerate(csv_reader):
            row_errors, date = self.process_csv_row(
                row, index, consumer_unit_id)

            if str(date) in seen_dates:
                row_errors["date"].append(DuplicatedDateError)

            seen_dates.add(str(date))
            energy_bill_row = {
                'consumer_unit': {'value': consumer_unit_id, 'error': False if consumer_unit_id else True},
                'date': {
                    'value': date,
                    'errors': row_errors.get('date')
                },
                'invoice_in_reais': {'value': row.get('invoice_in_reais', ""), 'errors': row_errors.get('invoice_in_reais')},
                'peak_consumption_in_kwh': {'value': row.get('peak_consumption_in_kwh', ""), 'errors': row_errors.get('peak_consumption_in_kwh')},
                'off_peak_consumption_in_kwh': {'value': row.get('off_peak_consumption_in_kwh', ""), 'errors': row_errors.get('off_peak_consumption_in_kwh')},
                'peak_measured_demand_in_kw': {'value': row.get('peak_measured_demand_in_kw', "") ,'errors': row_errors.get('peak_measured_demand_in_kw')},
                'off_peak_measured_demand_in_kw': {'value': row.get('off_peak_measured_demand_in_kw', ""), 'errors': row_errors.get('off_peak_measured_demand_in_kw')}
            }
            energy_bill_data.append(energy_bill_row)
            
        return energy_bill_data


    def validate_csv_row(self, row, consumer_unit_id):
        errors = defaultdict(list)
        date = ContractUtils().validate_date(row["date"])
        if(not isinstance(date, datetime.date)):
            errors["date"].append(FormatDateError)

        elif models.EnergyBill.check_energy_bill_month_year(consumer_unit_id, date):
            errors['date'].append(AlreadyHasEnergyBill)

        elif not models.EnergyBill.check_energy_bill_covered_by_contract(
            consumer_unit_id, date):
            errors["date"].append(DateNotCoverByContractError)

        for field, max_value in [
            ('invoice_in_reais', 99999999.99),
            ('peak_consumption_in_kwh', 9999999.99),
            ('off_peak_consumption_in_kwh', 9999999.99),
            ('peak_measured_demand_in_kw', 9999999.99),
            ('off_peak_measured_demand_in_kw', 9999999.99),
        ]:
            value = row.get(field, "")
            if isinstance(value, str):
                try: 
                    value = value.replace(',', '.')
                    if value > max_value:
                        errors[field].append(EnergyBillValueError if field=='invoice_in_reais' else ValueMaxError)
                    value = float(value)
                except: 
                    row[field] = value
                    if(value != ""):
                        errors[field].append(EnergyBillValueError if field=='invoice_in_reais' else ValueMaxError)
                        continue

            elif math.isnan(value):
                row[field] = ""

            else: 
                if value > max_value:
                    errors[field].append(EnergyBillValueError if field=='invoice_in_reais' else ValueMaxError)            

        if row.get('invoice_in_reais') == '':
            errors['invoice_in_reais'].append(EnergyBillValueError)

        return errors, date

    def process_csv_row(self, row, index, consumer_unit_id):
        row_errors, date = self.validate_csv_row(row, consumer_unit_id)
        return row_errors, date